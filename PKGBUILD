# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Contributor: Bebbum <bebbumman@gmail.com>
# Contributor: Anatol Pomozov <anatol.pomozov@gmail.com>
# Contributor: Jelle van der Waa

pkgname=hound
pkgver=0.7.1
pkgrel=2
pkgdesc='Lightning fast code searching made easy'
arch=('x86_64')
url='https://github.com/hound-search/hound'
license=('MIT')
depends=('glibc' 'git')
makedepends=('go') # no need for npm as the UI is pre-compiled
optdepends=(
  'breezy: for bazaar repositories'
  'mercurial: for mercurial repositories'
  'svn: for subversion repositories'
)
options=('!lto')
install=hound.install
source=(
  "$pkgname::git+$url#tag=v$pkgver"
  'systemd.service'
  'sysusers.conf'
  'tmpfiles.conf'
)
sha512sums=('6f06e0349f29d1ba690041ecd197317a82671ad7b637cae86520817744b49cd75bb7c6b510e1762b4d3141928d1bb89f018692c6db2582bda20d509f2fb14a6f'
            '341e423b1572dea500e9f914ef9bb9dfba7fc19a1cdba0d92e0ba5cf021150bda4322981920902f1ffcade222f26df808fafd681d29841b4892e43af1bd2ec1f'
            '4556b63ee2744d76aca7bdfc445f35f91409d4b15ffef53a1b3d378ae2b556e0db92415acd1f6aba03af0f010b8051f5035b4992f9777877403f5d2208f3bf59'
            '9a26a065237d3edd2f09d399ad1999276304db71398a7737b846a5e70eb0740baab4bdd56dfb225060c3ca995f4c13db9cfec1aebe1825473a6299484bdac5b9')
b2sums=('b930380f084145d974642ec6476416d8ffd101362b202ba9c860150821e6d773986847be7452d596e3fd00bffe8d86e41bc2c4098a0687696c01ec4a2494af9b'
        '09ba5894d734ae51126e515192e307b7ece3c5a6def93f5218b445c146e5081c6094f86449eadb724517c09c5f5b8d309b1bfea15b6c402a6b31b9c7366c3198'
        '07278ceec678fd3df24d2492fadec41f0cb6839d1d19fb77cc0e0c566b7e887d7855418cfb17f1a62cd476c17c2d93c3a74e8775ff645b9fe92218aaed2aff8b'
        '509003822d9bd826b5921d52483c79a5275b1da121e6f88bf66a644681c2a76a7abac5cf8241a094100c164f32e510da04b9f6037cf5e0910c98b062c68742e5')

prepare() {
  cd "$pkgname"

  # create directory for build output
  mkdir build

  # download dependencies
  export GOPATH="${srcdir}"
  go mod download
}

build() {
  cd "$pkgname"

  # set Go flags
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export GOPATH="${srcdir}"

  go build -v \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags "-compressdwarf=false \
    -linkmode external \
    -extldflags ${LDFLAGS}" \
    -o build \
    ./cmds/...
}

check() {
  cd "$pkgname"

  go test -v ./...
}

package() {
  # systemd integration
  install -vDm644 systemd.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -vDm644 sysusers.conf "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
  install -vDm644 tmpfiles.conf "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"

  cd "$pkgname"

  # binary
  install -vDm755 -t "$pkgdir/usr/bin" build/*

  # license
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE

  # documentation
  install -vDm644 -t "$pkgdir/usr/share/doc/$pkgname" \
    CONTRIBUTING.md README.md config-example.json default-config.json docs/*
}

